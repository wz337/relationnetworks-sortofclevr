import torch
from torch.autograd import Variable

import argparse
import os
import numpy as np
import random
import pickle

from model import CNN_MLP

def load_data():
    print ("Loading data now...")
    data_dir = "./sort-of-CLEVR"
    filename = os.path.join(data_dir, 'data.pickle')
    with open(filename, 'rb') as f:
        train, test = pickle.load(f)

    relation_train, nonrelation_train = process(train)
    relation_test, nonrelation_test = process(test)

    return relation_train, relation_test, nonrelation_train, nonrelation_test

def process(data):
    relation = []
    nonrelation = []

    for img, r, nr in data:
        img = np.swapaxes(img, 0, 2)
        for q, a in zip(r[0], r[1]):
            relation.append((img, q, a))
        for q, a in zip(nr[0], nr[1]):
            nonrelation.append((img, q, a))
    return relation, nonrelation

relation_train, relation_test, nonrelation_train, nonrelation_test = load_data()


# Training settings
parser = argparse.ArgumentParser(description='PyTorch Sort-of-CLEVR Example')
parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                    help='input batch size for training (default: 64)')
parser.add_argument('--epochs', type=int, default=20, metavar='N',
                    help='number of epochs to train (default: 10)')
parser.add_argument('--lr', type=float, default=0.0001, metavar='LR',
                    help='learning rate (default: 0.0001)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--resume', type=str,
                    help='resume from model stored')
parser.add_argument('--model', type=str, choices=['RN', 'CNN_MLP'], default='CNN_MLP',
                    help='resume from model stored')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

if args.model == 'CNN_MLP':
    model = CNN_MLP(args)
# else:
#     model = RN(args)

dir = './model'
batch_size = args.batch_size
img_tensor = torch.FloatTensor(batch_size, 3, 75, 75)
qst_tensor = torch.FloatTensor(batch_size, 11)
y = torch.LongTensor(batch_size)

if args.cuda:
    model.cuda()
    img_tensor = img_tensor.cuda()
    qst_tensor = qst_tensor.cuda()
    y = y.cuda()

img_tensor = Variable(img_tensor)
qst_tensor = Variable(qst_tensor)
y = Variable(y)

def create_tensor(data, i):
    left, right = batch_size*i, batch_size*(i+1)
    image = torch.from_numpy(np.asarray(data[0][left : right]))
    question = torch.from_numpy(np.asarray(data[1][left : right]))
    answer = torch.from_numpy(np.asarray(data[2][left : right]))

    img_tensor.data.resize_(image.size()).copy_(image)
    qst_tensor.data.resize_(question.size()).copy_(question)
    y.data.resize_(answer.size()).copy_(answer)

def divide_data(data):
    image = [d[0] for d in data]
    question = [d[1] for d in data]
    answer = [d[2] for d in data]
    return (image, question, answer)

def train(epoch, relation, nonrelation):
    model.train()

    random.shuffle(relation)
    random.shuffle(nonrelation)

    relation = divide_data(relation)
    nonrelation = divide_data(nonrelation)

    for index in range(len(relation[0]) // batch_size):
        create_tensor(relation, index)
        relation_accuracy = model.train_(img_tensor, qst_tensor, y)

        create_tensor(nonrelation, index)
        nonrelation_accuracy = model.train_(img_tensor, qst_tensor, y)

        if index % args.log_interval == 0:
            print('Epoch: {} [{}/{} ({:.0f}%)] Accuracy for Relation Question:\
                   {:.0f}%, Accuracy for Non-Relation Question: {:.0f}%'.format(epoch, index * batch_size * 2, len(relation[0] * 2),
                   100. * batch_size * index / len(relation[0]), relation_accuracy, nonrelation_accuracy))

def test(epoch, relation, nonrelation):
    model.eval()

    relation = divide_data(relation)
    nonrelation = divide_data(nonrelation)

    relation_accuracy = []
    nonrelation_accuracy = []

    for index in range(len[relation[0]]//batch_size):
        create_tensor(relation, index)
        relation_accuracy.append(model.test_(img_tensor, qst_tensor, y))

        create_tensor(nonrelation, index)
        nonrelation_accuracy.append(model.test_(img_tensor, qst_tensor, y))

    print('\n Evaluation: Accuracy for Relation Question: {:.0f}% | Accuracy for Non-Relation Question: {:.0f}%\n'.format(
        sum(relation_accuracy)/len(relation_accuracy), sum(nonrelation_accuracy)/len(nonrelation_accuracy)))

try:
    os.makedirs(dir)
except:
    print ('model already exists')

if args.resume:
    file = os.path.join(dir, args.resume)
    print ('loading checkpoint')
    checkpoint = torch.load(file)
    model.load_state_dict(checkpoint)
    print ('finishing loading')

for epoch in range(1, args.epochs + 1):
    train(epoch, relation_train, nonrelation_train)
    test(epoch, relation_test, nonrelation_test)
    model.size_model(epoch)