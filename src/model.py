from __future__ import print_function
import argparse
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()

        #3 input image channel, 24 output channels, 3 * 3 square convolution
        #kernel
        self.convlayer1 = nn.Conv2d(3, 24, 3, stride=2, padding=1)
        self.batch_norm1 = nn.BatchNorm2d(24)
        self.convlayer2 = nn.Conv2d(24, 24, 3, stride=2, padding=1)
        self.batch_norm2 = nn.BatchNorm2d(24)
        self.convlayer3 = nn.Conv2d(24, 24, 3, stride=2, padding=1)
        self.batch_norm3 = nn.BatchNorm2d(24)
        self.convlayer4 = nn.Conv2d(24, 24, 3, stride=2, padding=1)
        self.batch_norm4 = nn.BatchNorm2d(24)

    def forward(self, image):
        x = self.convlayer1(image)
        x = F.relu(x)
        x = self.batch_norm1(x)
        x = self.convlayer2(x)
        x = F.relu(x)
        x = self.batch_norm2(x)
        x = self.convlayer3(x)
        x = F.relu(x)
        x = self.batch_norm3(x)
        x = self.convlayer4(x)
        x = F.relu(x)
        x = self.batch_norm4(x)

        return x

class BaseModel(nn.Module):
    def __init__(self, args, name):
        super(BaseModel, self).__init__()
        self.name=name

    def train_(self, img_tensor, qst_tensor, y):
        self.optimizer.zero_grad()
        output = self(img_tensor, qst_tensor)
        loss = F.nll_loss(output, y)
        loss.backward()
        self.optimizer.step()
        prediction = output.data.max(1)[1]
        return prediction.eq(y.data).cpu().sum()/len(y) * 100

    def test_(self, img_tensor, qst_tensor, y):
        output = self(img_tensor, qst_tensor)
        prediction = output.data.max(1)[1]
        return prediction.eq(y.data).cpu.sum()/len(y) * 100

    def save_model(self, epoch):
        torch.save(self.state_dict(), 'model/epoch_{}_{:02d}.pth'.format(self.name, epoch))

class CNN_MLP(BaseModel):
    def __init__(self, args):
        super(CNN_MLP, self).__init__(args, 'CNNMLP')

        self.conv = CNN()
        self.fc1 = nn.Linear(5 * 5 * 24 + 11, 256)
        self.fcout = FCOutputModel()

        self.optimizer = optim.Adam(self.parameters(), lr=args.lr)

    def forward(self, image, question):
        x = self.conv(image)
        x = x.view(x.size(0), -1)

        x_ = torch.cat((x, question), 1)

        x_ = self.fc1(x_)
        x_ = F.relu(x_)

        return self.fcout(x_)

class FCOutputModel(nn.Module):
    def __init__(self):
        super(FCOutputModel, self).__init__()

        self.fc2 = nn.Linear(256, 256)
        self.fc3 = nn.Linear(256, 10)

    def forward(self, x):
        x = self.fc2(x)
        x = F.relu(x)
        x = F.dropout(x)
        x = self.fc3(x)
        return F.log_softmax(x)

# t0    random neurons (W0)
# t1 x1  forward (W0 . f(x1))   update W1
# t2 x2  forward (W1 . f(x2))   update W2
# validation W2