import cv2
import os
import pickle
import numpy as np
import random

"""question: [red, blue, green, orange, yellow, gray, nrq, rq, 1, 2, 3]"""
def display_data(data):
    img, q, a = data
    colors = ["red", "blue", "green", "orange", "yellow", "gray"]
    answers = ['yes', 'no', 'square', 'circle', '1', '2', '3', '4', '5', '6']

    for i in range(1):
        question = ''
        color = colors[q.tolist()[0:6].index(1)]
        subcat_idx = q.tolist()[9:12].index(1)

        #non-relational
        if q[6] == 1:
            if subcat_idx == 0:
                query = "What is the shape of the " + color + "object?"
            elif subcat_idx == 1:
                query = "Is the " + color + "object on the left or right of the image?"
            else:
                query = "Is the " + color + "object on the top or bottom of the image?"
        if q[7] == 1:
            if subcat_idx == 0:
                query = "What is the shape of the object that is the closest to the " + color + " object?"
            elif subcat_idx == 1:
                query = "What is the shape of the object that is the furtherest to the " + color + " object?"
            else:
                query = "How many objects have the shape of the " + color + " object?"

        answer = answers[a]
        qa = q + a
    id = str(random.randint(0,100000))
    cv2.imwrite(id + ".jpg", cv2.resize(img, (512, 512)))


def load_data():
    print ("Loading data now...")
    data_dir = "./sort-of-CLEVR"
    filename = os.path.join(data_dir, 'data.pickle')
    with open(filename, 'rb') as f:
        train, test = pickle.load(f)

    relation_train, nonrelation_train = process(train)
    relation_test, nonrelation_test = process(test)

    return relation_train, relation_test, nonrelation_train, nonrelation_test

def process(data):
    relation = []
    nonrelation = []

    for img, r, nr in data:
        img = np.swapaxes(img, 0, 2)
        for q, a in zip(r[0], r[1]):
            relation.append((img, q, a))
        for q, a in zip(nr[0], nr[1]):
            nonrelation.append((img, q, a))
    return relation, nonrelation

relation_train, relation_test, nonrelation_train, nonrelation_test = load_data()
print (len(relation_train))
print (relation_train[0])
print (type(relation_train[0][0]))
id = str(random.randint(0, 100000))
cv2.imshow('img', cv2.resize(relation_train[0][0], (512, 512)))
cv2.waitKey(0)
# cv2.imwrite(id + ".jpg", cv2.resize(relation_train[0][0], (512, 512)))
# rt = [display_data(d) for idx, d in enumerate(relation_train)]
