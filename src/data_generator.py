import os
import sys
import numpy as np
import pickle
import cv2
import random

data_size = 10000
val_ratio = 0.02
training_size = int(data_size * (1 - val_ratio))
validation_size = int(data_size - training_size)

#image: 75 * 75
img_size = 75
shape_size = 5

question_length = 11
number_of_question = 10
q_size = 10
rq_index = 7
nrq_index = 6

dir = './sort-of-CLEVR'
#rgb -> bgr
#rgb [(255, 0, 0), (0,0,255), (0,255,0), (255,165,0), (255,255,0), (128,128,128)]
#bgr values for red, blue, green, orange, yellow, gray
colors = [(0, 0, 255), (255, 0, 0), (0, 255, 0), (0, 165, 255), (0, 255, 255), (128, 128, 128)]


try:
    os.makedirs(dir)
except:
    print ("sort-of-CLEVR data already exists")

def generate_center(shapes):
    center = np.random.randint(0 + shape_size, img_size - shape_size, 2)
    if len(shapes) == 0:
        return center
    else:
        for other_center, _, _ in shapes:
            import math
            if math.sqrt(((center - other_center) ** 2).sum()) < (shape_size) * 2 * math.sqrt(2) + 1:
                return generate_center(shapes)
        return center

def build_data():
    shapes = []
    shape_option = ["square", "circle"]
    img = np.ones((img_size, img_size, 3)) * 255 #initialize blank image

    for idx, color in enumerate(colors):
        shape_center = generate_center(shapes)
        print (shape_center)
        if random.choice(shape_option) == "square":
            square_left_top = (shape_center[0] - shape_size, shape_center[1] - shape_size)
            square_right_bottom = (shape_center[0] + shape_size, shape_center[1] + shape_size)
            cv2.rectangle(img, square_left_top, square_right_bottom, color, -1)
            shapes.append((shape_center, color, "square"))
        else:
            circle_center = (shape_center[0], shape_center[1])
            cv2.circle(img, circle_center, shape_size, color, -1)
            shapes.append((shape_center, color, "circle"))

    relations = build_rq(shapes)
    non_relations = build_nrq(shapes)
    data = (img/255, relations, non_relations)
    # test images by saving them to files
    # id = str(random.randint(0,100000))
    # cv2.imwrite(id + ".jpg", cv2.resize(img, (512, 512)))
    return data

def build_rq(shapes):
    rq = []
    ra = []

    #gnereate relational questions
    for _ in range(number_of_question):
        question = np.zeros(question_length)
        random_color = random.randint(0, 5)
        question[random_color] = 1
        question[rq_index] = 1
        sub_categoires = random.randint(0, 2)
        question[len(question)-sub_categoires-1] = 1
        rq.append(question)

        if sub_categoires == 0:
            "What is the shape of the object that is closets to the xxx object?"
            the_shape_center = shapes[random_color][0]
            closet_idx = find_closest(shapes, the_shape_center)
            answer = sqaure_or_circle(shapes[closet_idx][2])

        elif sub_categoires == 1:
            "What is the shape of the object that is furthest to the xxx object?"
            the_shape_center = shapes[random_color][0]
            furthest_idx = find_furthest(shapes, the_shape_center)
            answer = sqaure_or_circle(shapes[furthest_idx][2])

        else:
            "How many objects have the shape of the xxx object?"
            my_shape_shape = shapes[random_color][2]
            count = -1
            for s in shapes:
                if s[2] == my_shape_shape:
                    count += 1
            answer = count + 4

        ra.append(answer)

    relations = (rq, ra)
    return relations

def build_nrq(shapes):
    nrq = []
    nra = []

    for _ in range(number_of_question):
        question = np.zeros(question_length)
        random_color = random.randint(0, 5)
        question[random_color] = 1
        question[nrq_index] = 1
        sub_categoires = random.randint(0, 2)
        question[len(question)-sub_categoires-1] = 1
        nrq.append(question)

        if sub_categoires == 0:
            "what is the shape of the xxx object"
            answer = sqaure_or_circle(shapes[random_color][2])

        elif sub_categoires == 1:
            "Is the xxx object on the left or right of the image?"
            if shapes[random_color][0][0] < img_size / 2:
                answer = 0
            else:
                answer = 1

        else:
            "How many objects have the shape of the xxx object?"
            if shapes[random_color][0][1] < img_size / 2:
                answer = 0
            else:
                answer = 1
        nra.append(answer)

    non_relations = (nrq, nra)
    return non_relations


def find_closest(shapes, the_shape_center):
    all_distance = [((cur_shape[0] - the_shape_center) ** 2).sum() for cur_shape in shapes]
    all_distance[all_distance.index(0)] = 99999
    return all_distance.index(min(all_distance))

def find_furthest(shapes, the_shape_center):
    all_distance = [((cur_shape[0] - the_shape_center) ** 2).sum() for cur_shape in shapes]
    all_distance[all_distance.index(0)] = -99999
    return all_distance.index(max(all_distance))

def sqaure_or_circle(s):
    if s == "square":
       return 2
    else:
       return 3

print ("Generating training set...")
training = [build_data() for _ in range(training_size)]
print ("Generating testing set...")
testing = [build_data() for _ in range(validation_size)]


print("Saving sort-of-CLEVR to data.pickle...")
filename = os.path.join(dir, "data.pickle")
with open(filename, 'wb') as file:
    pickle.dump((training, testing), file)
print ("Finished generating training and test set!")